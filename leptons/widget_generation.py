#!/usr/bin/env python

#   Copyright (C) 2021 by Andrea Calzavacca <paranoid.nemo@gmail.com>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the
#   Free Software Foundation, Inc.,
#   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

import os, sys

sys.path.insert(0, os.path.expanduser('~/projects/quantum'))

from quarks import methods as m
from quarks import check as c

# function definition

def create_widget():

    """(str) return a string containing a yuck widget to be used in a literal"""

    # variables definition

    store_file = "/tmp/store_notifications"

    #check if tmp file exists

    if not c.fl(store_file):
        raise FileNotFoundError()

    nty, nty_num = m.get_lines(store_file)

    # listing keys for widget construction
    # key_lst = ['app_name', 'summary', 'body', 'urgency', 'nty_id', 'progress']

    # start widget definition

    # crete header, always present
    rep_widget = '(box :class "notification_header" :orientation "h" (box :orientation "h" :space-evenly false (label :class "notification_header_num" :halign "start" :text "'+str(nty_num)+'")(label :class "notification_header_title" :halign "start" :text "Notifications"))(button :class "clear_all_button" :halign "end" :onclick "" "clear all"))'

    # if there are notifications add a bubble for every one in the file
    if nty_num > 0:
        for line in nty:
            line_info = line.split('&')
            yuck_widget_bubble = '(box :class "notification_bubble" :orientation "v" :space-evenly false (centerbox :class "notification_bubble_header" :orientation "h" :space-evenly false (label :class "notification_title" :halign "start" :limit-width 50 :text "'+line_info[0]+'")(label :text "")(button :class "discard_button" :halign "end" :onclick "" "x"))(label :class "notification_body" :halign "start" :wrap true :limit-width 100 :text "'+line_info[1]+'"))'
            rep_widget+=yuck_widget_bubble
    
    out_widget = out_widget = '(box :class "notification_container" :orientation "v" :space-evenly false '+rep_widget+')'

    print(out_widget)

if __name__ == "__main__":
    create_widget()