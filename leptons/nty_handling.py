#!/usr/bin/env python

#   Copyright (C) 2021 by Andrea Calzavacca <paranoid.nemo@gmail.com>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the
#   Free Software Foundation, Inc.,
#   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

import os, sys

sys.path.insert(0, os.path.expanduser('~/projects/quantum'))

from quarks import methods as m
from quarks import check as c

def get_data():

    """(list) return a list that contains all env variables gotten from dunst"""

    app_name = os.getenv('DUNST_APP_NAME')
    summary = os.getenv('DUNST_SUMMARY')
    body = os.getenv('DUNST_BODY')
    urgency = os.getenv('DUNST_URGENCY')
    nty_id = os.getenv('DUNST_ID')
    progress = os.getenv('DUNST_PROGRESS')

    value_lst = [app_name, summary, body, urgency, nty_id, progress]

    return(value_lst)

def write_data(file, in_list):

    """(void) write data on a defined file, create it if doesn't exists"""

    # variables definition
    sep = "&"

    # verify file existence
    if not c.fl(file):
        with open(file, "w") as f:
            f.seek(0)
            f.write()

    # write data on file
    m.append_new_line(file, in_list, sep)

def nty_get():

    """(void) get information from system and write data on file"""

    store_file = "/tmp/store_notifications"

    nty_list = get_data()
    write_data(store_file, nty_list)


if __name__ == "__main__":
    nty_get()