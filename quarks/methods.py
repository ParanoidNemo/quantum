#!/usr/bin/env python

#   Copyright (C) 2021 by Andrea Calzavacca <paranoid.nemo@gmail.com>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the
#   Free Software Foundation, Inc.,
#   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

"""This module define a set of functions to avoid repetitive operations.

Global functions defined:
    - insert_data: generate a string from a format string an a dict
    - create_dict: generate a dictionary from a list
    - format_string: generate a format string from an external file"""

import os, re
import subprocess
from itertools import zip_longest

def extract_data(rep_dict):

    """(string, string) extract keys and values from dict and return them as separate strings"""

    k = re.compile("-".join([re.escape(k) for k in rep_dict.keys()]))

    v = re.compile("-".join([re.escape(v) for v in rep_dict.values()]))

    keys = k.pattern
    values = v.pattern

    return(keys, values)

def create_dict(rep_list):

    """(dict) return a dict from a given list. The dict keys are progressive int enveloped by curled brachets"""

    rep_dict = {}

    for index, item in enumerate(rep_list):
        #i = "{" + str(index) + "}"
        rep_dict["{%d}" %index] = item

    return(rep_dict)

def create_custom_dict(key_list, field_list, fill="n/a"):

    """(dict) return a dict from two given lists. The first list provide the keys for the dict, the second one the fields"""

    rep_dict = {}

    if len(key_list) < len(field_list):
        raise ValueError('field_list len must be =< of key_list len')

    for k,f in zip_longest(key_list, field_list, fillvalue=fill):
        rep_dict[k] = f
    
    return(rep_dict)

def format_html_string(format_file):

    """(string) return a html format string from a given file"""

    format_string = ''

    with open(os.path.expanduser(format_file)) as f:
        for line in f:
            if line.startswith('#'):
                continue
            format_string += line

    format_string = re.sub(r'>\s<', '><', format_string)
    format_string = re.sub(r'\n', '', format_string)

    return(format_string)

def time_convertion(seconds):

    """(tuple) convert seconds into minutes/hours, return a tuple (hours, minutes, seconds)"""

    minutes = 0
    hours = 0
    days = 0

    while seconds >= 60:
        minutes += 1
        seconds -= 60

    while minutes >= 60:
        hours += 1
        minutes -= 60

    while hours >= 24:
        days += 1
        hours -= 24

    return(days, hours, minutes, seconds)

def call_command(command):

    """(list) produce a list containing the stdout of a bash command execute with subprocess module"""

    proc = subprocess.run(command.split(), stdout=subprocess.PIPE, universal_newlines=True)
    return(proc.stdout)

def sostitute_line(file_, match, new_line):

    """(void) sostitute chosen line(s) with another one in a chosen file"""

    with open(file_, 'r+') as f:
        lines = f.readlines()
        f.seek(0)
        f.truncate()
        for line in lines:
            if line.startswith(match):
                line = line.replace(line, new_line)
            f.write(line)

def clear_file(file):

    """(void) completely wipe the content of a given file"""

    with open(file, 'r+') as f:
        f.seek(0)
        f.truncate()

def get_lines(file):

    """(list, int) for a given file return a list containing every line and an int indicating the length of the list"""

    with open(file, "r") as f:
        lines = f.readlines()

        return lines, len(lines)

def append_new_line(file, in_list, separator=" "):

    """(void) write a new line to a file from a given list"""

    with open(file, "a+") as f:
        f.seek(0)

        data = f.read()
        if len(data) > 0:
            f.write("\n")

        v = separator.join(in_list)

        f.write(v)